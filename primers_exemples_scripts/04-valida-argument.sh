#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció: exemples if
# $prog edat
#------------------------------

#valida args
ERR_ARGS=1
if [ $# -ne 2 ]; then
	echo "ERROR: numero arguments incorrecte"
	echo "USAGE: $0 num1 num2"
	exit $ERR_ARGS
fi
#xixa
echo "$1 $2"
exit 0
