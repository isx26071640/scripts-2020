#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 11/02/2020
# Descripció: copiar origen-file dir-desti
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3

# validar arguments
if [ $# -ne 2 ]; then
	echo "ERROR:"
	echo "USAGE: numero arguments incorrecte"
	exit $ERR_NARGS
fi
file=$1
dirDesti=$2
# validar que existeix origen
if ! [ -e $file ]; then
    echo "$file no existeix"
    echo "usage: $0 file dirDesti"
    exit $ERR_NOFILE
fi
# validar que existeix dir desti
if [ ! -d $dirDesti ]; then
    echo "dir $dirDesti no existeix"
    echo "usage: $0 file dirDesti" 
    exit $ERR_NODIR
fi

# copiar origen desti
cp $file $dirDesti


