#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció: iterar per un conjunt d'elements
# 
#mostrar i numerar llista d'arg
cont=0
for arg in $*
do
	cont=$((cont+1))
        echo "$cont: $arg"
done
exit 0


# nomes itera per la llista d'args
for arg in "$@"
do
        echo $arg
done
exit 0

#itera per el contingut d'una variable
llistat=$(ls)
for nom in $llistat
do
        echo $nom
done
exit 0

# iterar per el contingut d'una cadena
# nomes itera un cop
for nom in "pere pau marta anna"
do
        echo $nom
done
exit 0

#iterar per un conjuunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
	echo $nom
done
exit 0
