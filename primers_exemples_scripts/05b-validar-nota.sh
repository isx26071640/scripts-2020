#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 3/02/2020
# Descripció: valida nota aprovat o suspes
# $prog nota
#------------------------------

#valida args
ERR_ARGS=1
ERR_Nota=2
#si num args no es correcet plegar
if [ $# -ne 1 ]; then
	echo "ERROR: numero arguments incorrecte"
	echo "USAGE: $0 nota"
	exit $ERR_ARGS
fi
#si la nota no esta entre 0-10 plegar
nota=$1
if [ $nota -lt 0 -o $nota -gt 10 ]; then
	echo "ERROR: nota $nota no valida [0-10]"
	echo "USAGE: $0 nota"
	exit $ERR_Nota
fi
#xixa
echo "$nota"
if [ $nota -lt 5 ]; then
	msg="suspes"
else
	msg="aprovat"
fi
echo $msg
exit 0
