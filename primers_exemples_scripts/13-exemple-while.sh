#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 05/02/2020
# Descripció:
# mostrar arguments
#processa cada linea de un fitxer
#file=$1
#count=0
#while read -r line
#do
#	count=$((count+1))
 #       echo "$count: $line"
#done < $file
#exit 0
read -r line
count=1
while [ ! "$line" = "FI" ]
do
       echo "$count: $line" | tr '[:lower:]' '[:upper:]'
       read -r line
       count=$((count+1))
done
exit 0


read -r line
while [ ! "$line" = "FI" ]
do                     
	echo "$line"
	read -r line
done 
exit 0
while []
do
	
done
exit 0
#processa stdin
while read -r line
do
	echo $line
done
exit 0
#mostra args ennumerats
count=0
while [ -n "$1" ]
do
	count=$((count+1))
	echo "$count: $1"
	shift
done
exit 0
#mostra els arg enumerats decreixent
while [ -n "$1" ]
do
        echo "$#: $*"
        shift
done
exit 0
#mostra els args ennumerats decreixent
while [ $# -gt 0 ]
do
	echo "$#: $*"
	shift
done
exit 0
#contador de N a 0
count=$1
while [ $count -gt 0 ]
do
	echo -n "$count, "
	count=$((count-1))
done
exit 0
		


#mostra de 1-10
MAX=10
count=0
while [ $count -lt $MAX ]
do
	count=$((count+1))
	echo "$count"
done
exit 0
