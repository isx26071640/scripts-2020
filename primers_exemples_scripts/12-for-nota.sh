#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció: dir per cada nota si esta suspes,aprova,notable,exelent
status=0
# validar 1 o  mes args
if [ $# -lt 1 ]; then
	echo "error: argument insuficients"
	echo "usage nota..."
	exit 1
fi
# per cada nota
llistatNotes=$*
for nota in $llistatNotes
do
	# validar si esta entre 0-10
	if ! [ $nota -ge 0 -a $nota -le 10 ]; then
		echo "ERROR: nota $nota no valida [0-10]" >> /dev/stderr
		status=$((status+1))
     	
	
	elif [ $nota -lt 5 ]; then
		echo "$nota Suspès"
	elif [ $nota -lt 7 ]; then
	    	echo "$nota Aprovat"
	elif [ $nota -lt 9 ]; then
	     	echo "$nota Notable"
	else
	    	echo "$nota Excel·lent"
	fi
done
exit $status
