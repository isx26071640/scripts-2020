#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 11/02/2020
# Descripció: copiar origen-file dir-desti
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
 
# validar arguments
if [ $# -lt 2 ]; then
	echo "ERROR: args incorrectes"
	echo "USAGE: $0 file[...] dirDesti "
	exit $ERR_NARGS
fi
dirDesti=$(echo "$*" | sed 's/^.* //')
llistaFile=$(echo "$*" | sed 's/ [^ ]*$//')
#validar que existeix dir desti
if [ ! -d $dirDesti ]; then
    echo "dir $dirDesti no existeix"
    echo "usage: $0 file dirDesti" 
    exit $ERR_NODIR
fi
# copiar origen desti
for file in $llistaFile
do
    if ! [ -e $file ]; then
       echo "$file no existeix" >> /dev/stderr
       echo "usage: $0 file dirDesti" >> /dev/stderr
    fi
    cp $file $dirDesti
done 

