#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 10/02/2020
# Descripció: Validar que es rep un argument o que es un dir i llistar-ho
# per llistar el contingut amb un ls ja n'hi ha prou
# mostra un a un els elements del sl
# dir per cada element si es file, link o que
status=0
#validar args
if [ $# -lt 1 ];then
	echo "ERROR: n arguments no valid"
	echo "USAGE: use $0 dir"
	status=$((status+1))
fi
for dir in $*
do
	echo "--------------$dir-------------------"
	#validar si es un dir existent
	if [ ! -d $dir ]; then
		echo "ERROR: element $dir no existent" >> /dev/stderr
		echo "USAGE: $0 dir" >> /dev/stderr
		status=$((status+1))
	else		
		llistar=$(ls $dir)
		count=1
		for elem in $llistar
		do
			if [ -f $dir/$elem ]; then
				echo "$count: $elem >>> es un regular file"
			elif [ -L $dir/$elem ]; then
				echo "$count: $elem >>> es un link simbolic"
			elif [ -d $dir/$elem ]; then
				echo "$count: $elem >>> es un directori"
			else
				echo "$count: $elem >>> es una altra cosa"
			fi
			count=$((count+1))

		done
	fi
done
exit $status
