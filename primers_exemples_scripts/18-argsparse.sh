#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 17/02/2020
# Descripció: -a file -b -c -d edat -e arg[...]
# validar que hi hagi almenys 1 arg
if [ $# -lt 1 ]; then
    echo "ERROR: args incorrectes"
    echo "USAGE: $0 -a file -b -c -d edat -e arg[...] "
    exit 1
fi
opcions=""
args=""
while [ -n "$1" ]
do
    case $1 in 
        -a) 
            opcions="$opcions $1"
            file=$2
            shift
            ;;
        -d)
            opcions="$opcions $1"
            edat=$2
            shift
            ;;
        -[bce])
            opcions="$opcions $1"
            ;;
        *)
            args="$args $1"
            ;;
    esac
    shift
done
echo "edat: $edat file: $file"
echo "opcions: $opcions"
echo "arguments: $args"
exit 0
