#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció:
echo '$*:' $* #llista d'args
echo '$@:' $@ #llista d'args
echo '$#:' $# #numero d'arguments
echo '$0:' $0 #nom del programa
echo '$1:' $1 #primer argument
echo '$10:' ${10}  #amb les claus encapsulem el nom dels args
nom='pipug'
echo ${{nom}pelat
exit 0
