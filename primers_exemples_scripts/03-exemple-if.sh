#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció: exemples if
# $prog edat
#------------------------------

#valida args
ERR_ARGS=1
if [ $# -ne 1 ]; then
	echo "ERROR: numero arguments incorrecte"
	echo "USAGE: $0 edat"
	exit $ERR_ARGS
fi

#xixa
edat=$1
if [ $edat -ge 18 ] ; then
	echo "edat $edat es major d'edat"
fi
exit 0

