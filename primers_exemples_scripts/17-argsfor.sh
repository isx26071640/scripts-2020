#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció: prog.sh [-a -b -c -d -e -f] args[...]

# validar que hi hagi almenys 1 arg
if [ $# -lt 1 ]; then
    echo "ERROR: args incorrectes"
    echo "USAGE: $0 [-a -b -c -d -e -f] args[...] "
    exit 1
fi
opcions=""
args=""
for arg in $*
do
    case $arg in 
        "-a"|"-b"|"-c"|"-d"|"-e"|"-f")
            opcions="$opcions $arg"
            ;;
        *)
            args="$args $arg"
            ;;
    esac
done

echo "opcions: $opcions"
echo "arguments: $args"
#identificar les opcions
# identificar els arguments



