#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 29/01/2020
# Descripció: mkdir no generi sortida de cap tipus
# cada vegada que no es pugui crear missatge nostre stderr
# 0 si OK 1 si errnargs 2 si hi hagut error en algun dir
ERR_nargs=1
ERR_nomkdir=2
status=0
if [ $# -lt 1 ]; then
  echo "ERROR: falta arguments"
  echo "USAGE: $0 noudir[...]"
  exit $ERR_nargs
fi
for dir in $*
do
  mkdir $dir &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: $dir no s'ha pogut crear" >> /dev/stderr
    status=$ERR_nomkdir
  fi
done
exit $status

