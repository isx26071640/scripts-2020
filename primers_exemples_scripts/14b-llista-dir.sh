#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 10/02/2020
# Descripció: Validar que es rep un argument o que es un dir i llistar-ho
# per llistar el contingut amb un ls ja n'hi ha prou
# mostra un a un els elements del sl
#validar args
if [ $# -ne 1 ];then
	echo "error"
	echo "usage"
	exit 1
fi
dir=$1
#validar si es un dir
if [ ! -d $dir ]; then
	echo "error dir"
	echo "usage $0 dir"
	exit 2
fi

llistar=$(ls $dir)
count=1
for elem in $llistar
do
	echo "$count: $elem"
	count=$((count+1))
done

exit 0
