#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 09/03/2020
# Descripció: Funcions usuaris

function hola(){
  echo "hola"
  return 0
}
function dia(){
  date
  return 0
}
function suma(){
  echo "$(($1+$2))"
  exit 0
}

function showUser(){
  ERRNARGS=1
  ERR_NOLOGIN=2
  OK=0
  #1) valida rep 1 arg
  if [ $# -ne 1 ]; then
    echo "error: arguments no valids"
    echo "usage: showUser login"
    return $ERR_NARGS
  fi
  login=$1
  #2) validar que existeix el login
  line=$(egrep "^$login:" /etc/passwd)
  if [ -z $line ]; then
    echo "error $login no existeix"
    echo "usage: showUser login"
    return $ERR_NOLOGIN
  fi
  uid=$(echo "$line" | cut -d: -f3)
  gid=$(echo "$line" | cut -d: -f4)
  gecos=$(echo "$line" | cut -d: -f5)
  home=$(echo "$line" | cut -d: -f6)
  shell=$(echo "$line" | cut -d: -f7)
  echo -e "login: $login\nuid: $uid\ngid: $gid\ngekos: $gecos\nhome: $home\nshell: $shell"
  return $OK
}
function showUserGecos(){
  ERRNARGS=1
  ERR_NOLOGIN=2
  OK=0
  #1) valida rep 1 arg
  if [ $# -ne 1 ]; then
    echo "error: arguments no valids"
    echo "usage: showUser login"
    return $ERR_NARGS
  fi
  login=$1
  #2) validar que existeix el login
  line=""
  line=$(egrep "^$login:" /etc/passwd)
  if [ -z "$line" ]; then
    echo "error $login no existeix"
    echo "usage: showUser login"
    return $ERR_NOLOGIN
  fi
  gecos=$(echo "$line" | cut -d: -f5)
  nom=$(echo "$gecos" | cut -d, -f1)
  ofice=$(echo "$gecos" | cut -d, -f2)
  telOfi=$(echo "$gecos" | cut -d, -f3)
  telhome=$(echo "$gecos" | cut -d, -f4)
  echo "Nom: $nom"
  echo "Oficina: $ofice"
  echo "Telefon Oficina: $telOfi"
  echo "Telefon Casa: $telhome" 
  return $OK
}

function showGroup(){
  ERRNARGS=1
  ERR_NOLOGIN=2
  OK=0
  if [ $# -ne 1 ]; then
    echo "error: arguments no valids"
    echo "usage: showGroup gname"
  fi
  gname=$1
  line=""
  line=$(egrep "^$gname:" /etc/group)
  if [ -z "$line" ]; then
    echo "error $gname no existeix"
    echo "usage: showGroup gname"
    return $ERR_NOLOGIN
  fi
  gid=$(echo "$line" | cut -d: -f3)
  users=$(echo "$line" | cut -d: -f4)
  echo "GID: $gid"
  echo "USERS: $users"
  return $OK
}

function showUserGname(){
  ERRNARGS=1
  ERR_NOLOGIN=2
  OK=0
  #1) valida rep 1 arg
  if [ $# -ne 1 ]; then
    echo "error: arguments no valids"
    echo "usage: showUser login"
    return $ERR_NARGS
  fi
  login=$1
  #2) validar que existeix el login
  line=$(egrep "^$login:" /etc/passwd)
  if [ -z "$line" ]; then
    echo "error $login no existeix"
    echo "usage: showUser login"
    return $ERR_NOLOGIN
  fi
  uid=$(echo "$line" | cut -d: -f3)
  gid=$(echo "$line" | cut -d: -f4)
  gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
  gecos=$(showUserGecos $login)
  home=$(echo "$line" | cut -d: -f6)
  shell=$(echo "$line" | cut -d: -f7)
  echo -e "login: $login\nuid: $uid\ngid: $gid \nGNAME:$gname \ngekos: $gecos\nhome: $home\nshell: $shell"
  return $OK
}
                                                            

