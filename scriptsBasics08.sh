#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: Fer un programa que rep com a argument noms d̉usuari, si existeixen en el s
# sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
for user in $*
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo "$user"
  else
    echo "$user" >> /dev/stderr
  fi
done
exit 0
