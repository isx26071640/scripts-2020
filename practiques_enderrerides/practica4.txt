En tots els casos verificar el target actual. Llisteu els processos i els serveis.
[root@pingu ~]# systemctl get-default 
graphical.target

Configurar el sistema establint default.target a mode multi-user.target.
systemctl set-default multi-user.target 

Manualment canviar a graphical.target amb isolate.
isolate graphical.target 
Reiniciar el sistema i al grub etablir l’opció de iniciar en mode emergency.target.
al final de la linia del kernel afegim:
systemd.unit=emergency.target
Reiniciar i indicar al grub l’opció del kernel per iniciar a rescue.target.
systemd.unit=rescue.target

Canviar de target amb l’ordre isolate activant multi-user.target.
isolate multi-user.target

Restablit per defecte graphical.target i reiniciar el sistema.
systemctl set-default grapghical.target
reboot

Amb isolate indicar que es vol accedir al target poweroff.target.
isolate poweroff.target
9. Iniciar el sistema en mode emergency.target. Llistar els processos, l’arbre de
processos, els units i les dependencies. Cal indicar el password de root? Hi ha
multiples sessions de consola?
si cal la password de root i no hi ha multiples sessions

10. Iniciar el sistema en mode init=/bin/bash. Llistar els processos, l’arbre de processos,
els units i les dependencies. Cal indicar el password de root? Hi ha multiples
sessions de consola?
no cal indicar el passwd i no hi ha multiples sessions


11. Instal-leu els serveis httpd, gpm, xinetd i ssh.

12. Activeu els serveis httpd i ssh i configureu-los disabled. Verifiqueu que estan
actius. Reiniciar el sistema i observar que no estan Iniciar el sistema.
[root@pingu ~]# systemctl start httpd
[root@pingu ~]# systemctl start sshd
[root@pingu ~]# systemctl disable httpd
[root@pingu ~]# systemctl disable sshd

13. Amb els serveis inactius condfigurar-los (httpd i sshd) com a enabled. Verificar
que estan enables però inactius. Reiniciar el sistema i observar que estan actius
i enabled.
[root@pingu ~]# systemctl stop httpd
[root@pingu ~]# systemctl stop sshd
[root@pingu ~]# systemctl enable httpd
[root@pingu ~]# systemctl enable sshd


14. Fer mask del servei httpd i observar que no es pot fer enable. Fer unmask.
[root@pingu ~]# systemctl mask httpd
Created symlink /etc/systemd/system/httpd.service → /dev/null.
[root@pingu ~]# systemctl enable httpd
Failed to enable unit: Unit file /etc/systemd/system/httpd.service is masked.
[root@pingu ~]# systemctl status httpd
● httpd.service
   Loaded: masked (/dev/null; masked)
   Active: active (running) since Sun 2020-06-07 00:05:51 CEST; 16min ago
 Main PID: 6911 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4915)
   CGroup: /system.slice/httpd.service
           ├─6911 /usr/sbin/httpd -DFOREGROUND
           ├─6912 /usr/sbin/httpd -DFOREGROUND
           ├─6914 /usr/sbin/httpd -DFOREGROUND
           ├─6915 /usr/sbin/httpd -DFOREGROUND
           └─6917 /usr/sbin/httpd -DFOREGROUND

Jun 07 00:05:49 pingu systemd[1]: Starting The Apache HTTP Server...
Jun 07 00:05:51 pingu httpd[6911]: Server configured, listening on: port 80
Jun 07 00:05:51 pingu systemd[1]: Started The Apache HTTP Server.
Jun 07 00:22:28 pingu systemd[1]: httpd.service: Current command vanished from the unit fil
Jun 07 00:22:30 pingu systemd[1]: httpd.service: Got notification message from PID 6911, bu
[root@pingu ~]# systemctl unmask httpd
Removed /etc/systemd/system/httpd.service.
[root@pingu ~]# systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.


15. Identificar el fitxer .service del servei xinetd. Identificar l’executable.
[root@pingu ~]# systemctl list-unit-files | egrep ".service" | grep xinetd
xinetd.service                                             enabled        
[root@pingu ~]# rpm -ql xinetd | egrep "bin|sbin"
/usr/sbin/xinetd

16. Configurar manualment amb symlinks el enable del servei xinetd i verificar-ho.
[root@pingu ~]# ln -s /usr/lib/systemd/system/xinetd.service /etc/systemd/system/multi-user.target.wants/xinetd.service.
[root@pingu ~]# systemctl is-enabled xinetd
enabled


17. Eliminar el enable del servei xinetd manualment amb el symlink. Verificar-ho.
[root@pingu ~]# rm /etc/systemd/system/multi-user.target.wants/xinetd.service.
rm: remove symbolic link '/etc/systemd/system/multi-user.target.wants/xinetd.service.'? y
[root@pingu ~]# systemctl is-enabled xinetd
disabled


18. Instal·la el paquet xinetd. Llista els seus components.
[root@pingu ~]# rpm -ql xinetd
/etc/xinetd.conf
/etc/xinetd.d/chargen-dgram
/etc/xinetd.d/chargen-stream
/etc/xinetd.d/daytime-dgram
/etc/xinetd.d/daytime-stream
/etc/xinetd.d/discard-dgram
/etc/xinetd.d/discard-stream
/etc/xinetd.d/echo-dgram
/etc/xinetd.d/echo-stream
/etc/xinetd.d/tcpmux-server
/etc/xinetd.d/time-dgram
/etc/xinetd.d/time-stream
/usr/lib/.build-id
/usr/lib/.build-id/11
/usr/lib/.build-id/11/1ad5da9ceb75bc74420fcb5cfaf184ce194ccf
/usr/lib/systemd/system/xinetd.service
/usr/sbin/xinetd
/usr/share/doc/xinetd
/usr/share/doc/xinetd/CHANGELOG
/usr/share/doc/xinetd/COPYRIGHT
/usr/share/doc/xinetd/README
/usr/share/doc/xinetd/empty.conf
/usr/share/doc/xinetd/sample.conf
/usr/share/man/man5/xinetd.conf.5.gz
/usr/share/man/man5/xinetd.log.5.gz
/usr/share/man/man8/xinetd.8.gz

19. Identifica el fitxer de servei de xinetd i llista’l. Identifica l’executable del servei i a quin target s’instal·la.
[root@pingu ~]# rpm -ql xinetd | egrep ".service$"
/usr/lib/systemd/system/xinetd.service
[root@pingu ~]# cat $(rpm -ql xinetd | egrep ".service$")
[Unit]
Description=Xinetd A Powerful Replacement For Inetd
After=syslog.target network.target
Documentation=man:xinetd
Documentation=man:xinetd.conf
Documentation=man:xinetd.log

[Service]
Type=forking
PIDFile=/var/run/xinetd.pid
ExecStart=/usr/sbin/xinetd -stayalive -pidfile /var/run/xinetd.pid
ExecReload=/usr/bin/kill -HUP $MAINPID

[Install]
WantedBy=multi-user.target

20. Llista el directori on hi ha els fitxers de les units de servei de multi-user.target.
Identifica el fitxer corresponent al servei xientd.

21. Fes enable del servei xinetd. S’ha creat un link. Llista el directori origen i el
[root@pingu ~]# ls -l /etc/systemd/system/multi-user.target.wants/xinetd.service
lrwxrwxrwx. 1 root root 38 Jun  7 00:58 /etc/systemd/system/multi-user.target.wants/xinetd.service -> /usr/lib/systemd/system/xinetd.service
directori destí del link. On s’ha creat el link? Perquè?
[root@pingu ~]# systemctl enable xinetd
Created symlink /etc/systemd/system/multi-user.target.wants/xinetd.service → /usr/lib/systemd/system/xinetd.service.
[root@pingu ~]# ls -l /usr/lib/systemd/system/xinetd.service
-rw-r--r--. 1 root root 375 Aug  5  2017 /usr/lib/systemd/system/xinetd.service
[root@pingu ~]# 

22. Llista les dependències de multi-user.target: totes; només els targets; només els
serveis.
[root@pingu ~]# systemctl list-dependencies multi-user.target
[root@pingu ~]# systemctl list-dependencies multi-user.target | grep ".service$"
[root@pingu ~]# systemctl list-dependencies multi-user.target | grep ".target$"

23. Llista les dependències de xinetd.service.
[root@pingu ~]# systemctl list-dependencies xinetd.service
xinetd.service
● ├─system.slice
● └─sysinit.target
●   ├─dev-hugepages.mount
●   ├─dev-mqueue.mount
●   ├─dracut-shutdown.service
●   ├─fedora-import-state.service
[...]
24. Fes l’ordre systemctl show del servei xinetd i identifica els elements: After i
WantedBy.
[root@pingu ~]# systemctl show xinetd.service | egrep -i "^after|^wantedby"
WantedBy=multi-user.target
After=systemd-journald.socket syslog.target sysinit.target network.target basic.target system.slice

25. Es pot iniciar sessió en el sysinit.target? Explica el perquè documentant-ho.

26. Mostra les dependències de recue.target: totes; targets; serveis.
[root@pingu ~]# systemctl list-dependencies rescue.target 
rescue.target
● ├─rescue.service
● ├─systemd-update-utmp-runlevel.service
● └─sysinit.target
●   ├─dev-hugepages.mount
●   ├─dev-mqueue.mount
●   ├─dracut-shutdown.service
●   ├─fedora-import-state.service

[root@pingu ~]# systemctl list-dependencies rescue.target | grep ".target$"
rescue.target
● └─sysinit.target
●   ├─cryptsetup.target
●   ├─local-fs.target
●   └─swap.target

[root@pingu ~]# systemctl list-dependencies rescue.target | grep ".service$"
● ├─rescue.service
● ├─systemd-update-utmp-runlevel.service
●   ├─dracut-shutdown.service
●   ├─fedora-import-state.service
●   ├─iscsi.service
●   ├─kmod-static-nodes.service
[...]
27. Mostra les dependències de emergency.target: totes; targets; serveis.
[root@pingu ~]# systemctl list-dependencies emergency.target 
emergency.target
● └─emergency.service
[root@pingu ~]# systemctl list-dependencies emergency.target | grep ".service$"
● └─emergency.service
[root@pingu ~]# systemctl list-dependencies emergency.target | grep ".target$"
emergency.target

28. Estableix com a target per defecte reboot.target i reinicia la màquina.
[root@pingu ~]# #systemctl set-default reboot.target
[root@pingu ~]# 
29. Punyeta! arregla-ho!
En el grub modifiquem la linea del kernel i afegim que inicii amb
rescue.target mateix i posem per defecte el que estaba, en el meu cas
graphical.target

