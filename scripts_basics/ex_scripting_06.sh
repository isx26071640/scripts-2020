#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: Processar per stdin linies d̉entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
while read -r line
do
  echo "$line" | sed -r 's/^(.)[^ ]* /\1. /'
done
exit 0
