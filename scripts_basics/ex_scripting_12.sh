#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 02/03/2020
# Descripció:Per a cada uid mostra la informació de l̉usuari en format:: 
# login(uid) gname home shell
for uid in $*
do
  user=$(egrep "^[^:]*:[^:]*:$uid:" /etc/passwd | cut -d: -f1)
  gid=$(egrep "^[^:]*:[^:]*:$uid:" /etc/passwd | cut -d: -f4)
  gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
  home=$(egrep "^[^:]*:[^:]*:$uid:" /etc/passwd | cut -d: -f6)
  shell=$(egrep "^[^:]*:[^:]*:$uid:" /etc/passwd | cut -d: -f7)
  echo "$user($uid) $gname $home $shell"
done
exit 0

