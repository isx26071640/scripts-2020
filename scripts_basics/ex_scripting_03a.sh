#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: llistar les vàlides, del tipus: 9999-AAA.
for mat in $*
do
  echo $mat | egrep "^[0-9]{4}-[A-Z]{3}$"
done
exit 0
