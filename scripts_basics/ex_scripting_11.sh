#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 20/02/2020
# Descripció: Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: g
# gname: GNAME, gid: GID, users: USERS
for gid in $*
do
  gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
  users=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
  echo "gname: $gname, gid: $gid, users: "$users" "
done
exit 0


