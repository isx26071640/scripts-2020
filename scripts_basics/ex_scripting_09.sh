#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 20/02/2020
# Descripció:Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
# No cal validar ni mostrar res!
# $ prog.sh -e 18 -r -c puig -j wheel postgres ldap >>> opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»
# Help: prog file…
if [ $1 = "-h" -o $1 = "--help" ]; then
  echo "Help for $0 by ASIX-M01 EdT"
  echo "Programa mostra opcions, variables, arguments"
  echo "Usage: $0 [ -r -m -c cognom  -j  -e edat ]  arg…"
  exit 0
fi
opcions=""
cognom=""
arguments=""
while [ -n "$1" ]
do
  case $1 in
    -[rmj])
      opcions="$opcions $1";;
    "-c")
      cognom=$2
      shift;;
    "-e")
      edat=$2
      shift;;
      *)
        arguments="$arguments $1";;
    esac
  shift
done
echo "opcions <$opcions>, cognom <$cognom>, edat<$edat>, arguments<$arguments>"
exit 0
