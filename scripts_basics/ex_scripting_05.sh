#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
while read -r line
do
  echo $line | egrep  "^.{0,50}$"
done
exit 0
