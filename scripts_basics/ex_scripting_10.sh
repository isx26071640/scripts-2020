#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 20/02/2020
# Descripció: Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: 
# gname: GNAME, gid: GID, users: USERS
while read -r gid
do
  gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
  users=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
  echo "gname: $gname, gid: $gid, users: "$users""
done
exit 0


