#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: Processar stdin mostrant per stdout les línies numerades i en majúscules.
count=0
while read -r line
do
  line=$(echo $line | tr '[:lower:]' '[:upper:]')
  count=$((count+1))
  echo "$count: $line"
done
exit 0
