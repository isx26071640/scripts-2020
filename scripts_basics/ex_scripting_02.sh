#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters. 
count=0
for arg in $*
do
  echo $arg | egrep "^.{3}" &> /dev/null
  if [ $? -eq 0 ]; then
    count=$((count+1))
  fi
done
echo "$count"
exit 0
