#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
for arg in $*
do
  echo $arg | egrep "^.{4}"
done
exit 0
