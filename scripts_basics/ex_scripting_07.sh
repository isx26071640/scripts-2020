#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 20/02/2020
# Descripció: Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida
# amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
# Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
# Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.

#7b ampliat a -h o --help
if [ $1 = "-h" -o $1 = "--help" ]; then
  echo "Help for $0 by ASIX-M01 EdT"
  echo "Programa valida si dirs o files"
  echo "Usage: $0 -f|-d arg1 arg2 arg3 arg4"
  exit 0
fi
#7a programa
status=0
#validar n args
if [ $# -ne 5 ]; then
  echo "error: args no valids"
  echo "usage: $0 -f|-d arg1 arg2 arg3 arg4" 
  exit 1
fi
#validar que el primer arg sigui -d o -f
if ! [ "$1" = "-d" -o "$1" = "-f" ]; then
  echo "error: args no valids"
  echo"usage: -d|-f arg1 arg2 arg3 arg4"
  exit 2
#agafar el tipus en una variable
type_args=$1
shift
#comprovem els args siguin files o dirs.
for elem in $*
do
  if ! [ $type_args $elem ]; then
    status=2
  fi
done
exit $status
