#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 19/02/2020
# Descripció: stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d̉errors (de no vàlides)
status=0
for mat in $*
do
  echo $mat | egrep "^[0-9]{4}-[A-Z]{3}$"
  if [ $? -ne 0 ]; then
    echo $mat >> /dev/stderr
    status=$((status+1))
  fi
done
exit $status
