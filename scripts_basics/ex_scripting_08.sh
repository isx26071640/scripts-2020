#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 20/02/2020
# Descripció: Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o
# o un missatge d̉error per stderror si no s̉ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
# Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
# Help: prog file…
if [ $# -eq 1 -a \( "$1" = "-h" -o "$1" = "--help" \) ]; then
  echo "Help for $0 by ASIX-M01 EdT"
  echo "Programa comprimeix files"
  echo "Usage: $0 file..."
  exit 0
fi

status=0
if [ $# -lt 1 ]; then
  echo "error: missing args"
  echo "usage: $0 file..."
  exit 1
fi
count=0
for file in $*
do
  tar cvzf $file.tar $file &> /dev/null
  if [ $? -eq 0 ]; then
    echo "$file"
    count=$((count+1))
  else
    echo "$file no s'ha pogut comprimir" >> /dev/stderr
    status=2
  fi
done
echo "files comprimits $count"
exit $status
