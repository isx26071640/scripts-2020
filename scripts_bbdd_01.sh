#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Scripting Bàsic
# Mati Vizcaíno
# 26/02/2020
# Descripció: Llistar usuaris del sistema en un format determinat
# "login -- uid -- gid -- home"
cut -d: -f1,3,4,6 /etc/passwd | sed -r 's/:/ -- /g'

