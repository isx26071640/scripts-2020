#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
#processos d'un usuari i com matarlos
pkill -u user1
ps -u $USER
ps -u user1 -o pid
#treballs d'impresio i com matarlos
lprm -U $USER
#tasques periodiques i com matarles
crontab -r -u $USER
rm -rf $(find /var/spool/at -user user1)
#fitxer fora del home
find / -user $USER
#moure tot el home d'un usuari a un tar.gz
#[user1@pingu ~]$ tree
#.
#├── file1.txt
#├── file2.txt
#├── file3.txt
#└── prova
#    ├── file4.pdf
#    ├── file5.pdf
#    └── file6.png
#1 directory, 6 files
tar cvzf home-$USER.tar.gz ~$USER
