#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
# Descripció: crea-clase-hardcoded.sh group
# crea un conjunt d'usuaris i rep com argument un grup
echo "- Primer punt"
echo "/etc/enviroment /etc/profile /etc/profile.d /etc/bashrc"
echo "- Segon punt"
echo " ~/.bashrc ~/.bash_profile ~/.bash_logout"
echo "- Tercer punt"
echo "~/.bash_profile"
echo "- Quart punt"
echo "~/.bashrc"

