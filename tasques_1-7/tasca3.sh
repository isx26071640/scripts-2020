#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
# Descripció: crea-clase-hardcoded.sh group
# crea un conjunt d'usuaris i rep com argument un grup
ERR_nargs=1
if [ $# -ne 1 ]; then
  echo "ERROR: nº args incorrecte"
  echo "usage: $0 group-name"
  exit $ERR_nargs
fi
group=$1
echo "groupadd $group"
alumList=$(echo $group-{00..30})
for alumne in $alumList
do
  echo "useradd -g $group -G users -b /home/inf/hisx1/ -p alum $alumne"
done
exit 0

