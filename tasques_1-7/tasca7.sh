#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
echo"- Primer punt"
echo "editem el fitxer /etc/profile, afegim les variables i les exportem\
  i amb els alies igual"
echo "- Segon punt"
echo "editem el fitxer ~/.bashrc i definim la variable i el alies, els canvis\
  es faran quan l'usuari torni a iniciar sessio"
echo "- Tercer punt"
echo "no podem editar el fitxer com a usuaris sense privilegis, aixi que el\
  que podem fer es redefinir la variable o l'alies en el nostre propi fitxer\
  ja que te preferencia en el path"

