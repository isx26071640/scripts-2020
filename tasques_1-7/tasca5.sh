#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
# Descripció: crea-clase-random.sh group login[...]
# crea un conjunt d'usuaris i rep com argument un grup i un o mes logins
# crea fitxer passwd.log amb usuaris i contrasenyes format login:passwd
ERR_nargs=1
if [ $# -lt 2 ]; then
  echo "ERROR: nº args incorrecte"
  echo "usage: $0 group-name login[...]"
  exit $ERR_nargs
fi
groupName=$1
shift
echo "groupadd $groupName"
for login in $*
do
  password=$(tr -dc A-Za-z0-9 < /dev/urandom | head -c8)
  echo "useradd -g $groupName -p $password $login"
  echo "$login:$password" >> passwd.log
done
exit 0
