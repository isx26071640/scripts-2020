#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
# Descripció: del_usuari.sh login
# elimina un usuari i tot el que li pertany
ERR_nargs=1
if [ $# -ne 1 ]; then
  echo "ERROR: nº args incorrecte"
  echo "usage: $0 login"
  exit $ERR_nargs
fi
login=$1
egrep "^$login:" /etc/passwd &> /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: usuari $login no existeix"
  echo "usage: $0 login"
  exit 2
fi

#eliminar processos del usuari
ps -u $login &> /dev/null 
if [ $? -eq 0 ]; then
  pkill -u $login &> /dev/null
  echo "s'han eliminat els processos del usuari $login" >> /dev/stderr
fi
#eliminar cua d'impresio
lpq -u $login &> /dev/null
if [ $? -eq 0 ]; then
  lprm -u $login &> /dev/null
  echo "s'ha eliminat la cua d'impresio" >> /dev/stderr
fi
#eliminar crontab
crontab -lu $login &> /dev/null
if [ $? -eq 0 ]; then
  crontab -ru $login &> /dev/null
  echo "s'ha eliminat el crontab de l'usuari $login" >> /dev/sdterr
fi
#crear tar amb home + files
tar cvzf /tmp/user1.tar.gz $(find / -user $login) ~$login
#eliminar l'usuari
userdel -r $login
exit 0
