#! /bin/bash
# ESCOLA DEL TREBALL ASIX-M01
# UF2: Administracio d'usuaris
# Mati Vizcaíno
# 11/04/2020
# Descripció: crea-clase-file.sh group file
# crea un conjunt d'usuaris i rep com argument un grup i un file format login:password
ERR_nargs=1
ERR_nofile=2
if [ $# -ne 2 ]; then
  echo "ERROR: nº args incorrecte"
  echo "usage: $0 nom-clase file"
  exit $ERR_nargs
fi
group=$1
file=$2
if ! [ -f $file ]; then
  echo "ERROR: fitxer $file no existeix"
  echo "usage: $0 nom-clase file"
  exit $ERR_nofile
fi
echo "groupadd $group"
while read -r line
do
  login=$(echo $line | cut -d: -f1)
  passwd=$(echo $line | cut -d: -f2)
  echo "useradd -g $group -p $passwd $login"
done < $file
exit 0
